defmodule OciManjaro.MixProject do
  use Mix.Project

  def project do
    [
      app: :oci_manjarolinux,
      version: "0.1.0-dev",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:buildah, git: "https://gitlab.com/oci-container-images4automated-tests/elixir-buildah.git", tag: "0.1.0-dev"},
      {:container_image_util, git: "https://gitlab.com/oci-container-images4automated-tests/container-image-util.git", tag: "0.1.0-dev"},
      # {:podman, git: "https://gitlab.com/oci-container-images4automated-tests/elixir-podman.git", tag: "0.1.0-dev"}
      {:oci_catatonit, git: "https://gitlab.com/oci-container-images4automated-tests/catatonit.git", tag: "0.1.0-dev"},
      {:oci_moreutils, git: "https://gitlab.com/oci-container-images4automated-tests/moreutils.git", tag: "0.1.0-dev"},
      {:oci_sudo, git: "https://gitlab.com/oci-container-images4automated-tests/sudo.git", tag: "0.1.0-dev"},
      {:oci_hlint, git: "https://gitlab.com/oci-container-images4automated-tests/hlint.git", tag: "0.1.0-dev"},
      {:oci_sqlite, git: "https://gitlab.com/oci-container-images4automated-tests/sqlite.git", tag: "0.1.0-dev"},
      {:oci_php, git: "https://gitlab.com/oci-container-images4automated-tests/php.git", tag: "0.1.0-dev"}
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
    ]
  end
end

