alias Buildah.Pacman.{
    Catatonit,
    Hlint,
    Moreutils,
    # Php,
    Sqlite,
    Sudo
}

alias ContainerImageUtil

defmodule OciManjaro do

    def make_manjaro_images(
            from_registry,
            from_name,
            to_registry,
            options \\ []
        ) do
        options = Keyword.merge([
                tag: nil #,
                # repositories_to_add: []
            ],
            options
        )
        tag = options[:tag]
        options = Keyword.merge([from_tag: tag], options)
        from_tag = options[:from_tag]
        # repositories_to_add = options[:repositories_to_add]
        # ci_registry_image = System.fetch_env!("CI_REGISTRY_IMAGE")
        # destination_registry = System.fetch_env!("DESTINATION_REGISTRY")
        quiet = System.get_env("QUIET")

        # manjaro/catatonit
        Buildah.from_push(
            ContainerImageUtil.image_name(
                from_registry, from_name, from_tag),
            &Catatonit.on_container/2,
            &Catatonit.test/3,
            image_to: ContainerImageUtil.image_name(
                to_registry, "catatonit", tag),
            quiet: quiet
        )

        # manjaro/moreutils
        Buildah.from_push(
            ContainerImageUtil.image_name(
                to_registry, "catatonit", tag),
            &Moreutils.on_container/2,
            &Moreutils.test/3,
            image_to: ContainerImageUtil.image_name(
                to_registry, "moreutils", tag),
            quiet: quiet
        )

        # manjaro/sudo
        Buildah.from_push(
            ContainerImageUtil.image_name(
                to_registry, "moreutils", tag),
            &Sudo.on_container_adding_user_user/2,
            &Sudo.test/3,
            image_to: ContainerImageUtil.image_name(
                to_registry, "sudo", tag),
            quiet: quiet
        )

        ## alpine/flatpak
        # apk_flatpak(
            # ci_registry_image, "sudo",
            # ci_registry_image, "flatpak",
            # tag: tag,
            # push: true
        # )

        ## alpine/openrc
        # apk_openrc(
            # ci_registry_image, "sudo",
            # ci_registry_image, "openrc",
            # tag: tag,
            # push: true
        # )

        ## alpine/mariadb
        # apk_mariadb(
            # ci_registry_image, "openrc",
            # ci_registry_image, "mariadb",
            # tag: tag,
            # push: true
        # )

        ## alpine/postgresql
        # apk_postgresql(
            # ci_registry_image, "openrc",
            # ci_registry_image, "postgresql",
            # tag: tag,
            # push: true
        # )

        # manjaro/hlint
        Buildah.from_push(
            ContainerImageUtil.image_name(
                to_registry, "sudo", tag),
            &Hlint.on_container/2,
            &Hlint.test/3,
            image_to: ContainerImageUtil.image_name(
                to_registry, "hlint", tag),
            quiet: quiet
        )

        # manjaro/sqlite
        Buildah.from_push(
            ContainerImageUtil.image_name(
                to_registry, "sudo", tag),
            &Sqlite.on_container/2,
            &Sqlite.test/3,
            image_to: ContainerImageUtil.image_name(
                to_registry, "sqlite", tag),
            quiet: quiet
        )

        ## alpine/php-pdo_mysql
        # apk_php_pdo_mysql(
            # ci_registry_image, "mariadb",
            # ci_registry_image, "php-pdo_mysql",
            # tag: tag,
            # push: true
        # )

        ## alpine/php-pdo_pgsql
        # apk_php_pdo_pgsql(
            # ci_registry_image, "postgresql",
            # ci_registry_image, "php-pdo_pgsql",
            # tag: tag,
            # push: true
        # )

        # alpine/php-pdo_sqlite
        # Buildah.from_push(
            # ContainerImageUtil.image_name(
                # to_registry, "sqlite", tag),
            # &Php.Pdo.Sqlite.on_container/2,
            # &Php.Pdo.Sqlite.test/3,
            # image_to: ContainerImageUtil.image_name(
                # to_registry, "php-pdo_sqlite", tag),
            # quiet: quiet
        # )
  end

end

